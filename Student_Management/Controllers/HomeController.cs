﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using Model;


namespace Student_Management.Controllers
{
    public class HomeController : Controller
    {
        // ./Home/Index
        // ./Home/
        private Alumno alumno = new Alumno();
        private AlumnoCurso alumnoCurso = new AlumnoCurso();
        private Curso curso = new Curso();
        private Adjunto adjunto = new Adjunto();

        public ActionResult Index()
        {
            return View(alumno.GetList());
        }

        // ./Home/Show
        public ActionResult Show(int id)
        {
            return View(alumno.GetOne(id));
        }

        // ./Home/Courses/?Alumno_id=1
        public PartialViewResult Courses(int idAlumno)
        {
            //LISTING COURSES FOR ONE STUDENT
            ViewBag.CoursesSelected = alumnoCurso.ByStudent(idAlumno);
            
            //LISTING ALL COURSES AVAILABLE
            ViewBag.Courses = curso.GetAll(idAlumno);
            
            //MODEL
            alumnoCurso.Alumno_id = idAlumno;

            return PartialView(alumnoCurso);
        }

        // ./Home/Attached/?Alumno_id=1
        public PartialViewResult Attached(int idAlumno)
        {
            ViewBag.Attaches = adjunto.GetAll(idAlumno);
            adjunto.Alumno_id = idAlumno;
            return PartialView(adjunto);
        }

        public ActionResult Crud(int id = 0)
        {
            return View(id == 0? new Alumno() : alumno.GetOne(id));
        }

        public JsonResult Save(Alumno model)
        {
            var rm = new ResponseModel();
            if (ModelState.IsValid)
            {
                rm = model.Save();
                if (rm.response)
                {
                    //rm.function = "DoSomething()";
                    rm.href = Url.Content("~/Home");
                }
            }
            return Json(rm);
        }

        public JsonResult SaveCourses(AlumnoCurso model)
        {
            var rm = new ResponseModel();
            if (ModelState.IsValid)
            {
                rm = model.Save();
                if (rm.response)
                {
                    //rm.function = "DoSomething()";
                    rm.function = "LoadCourses()";
                }
            }
            return Json(rm);
        }

        public ActionResult Delete(int id)
        {
            alumno.id = id;
            alumno.Delete();
            return Redirect("~/Home");
        }

        public JsonResult DeleteCourse(int id)
        {
            var rm = new ResponseModel();
            if (ModelState.IsValid)
            {
                rm = alumnoCurso.Delete(id);
                if (rm.response)
                {
                    rm.function = "LoadCourses()";
                }
            }
            return Json(rm);
        }

        public JsonResult SaveAttached(Adjunto model, HttpPostedFileBase Archivo)
        {
            var rm = new ResponseModel();
            if (Archivo != null)
            {
                //FILE UPLOAD IS RE-NAME TO NEVER REPEAT ANY NAME
                string name_attach = DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(Archivo.FileName);
                //ROUTE WHERE THE FILE IT WILL BE SAVE
                Archivo.SaveAs(Server.MapPath("~/Uploads/"+name_attach));
                //SET TO OUR MODEL THE NAME OF FILE
                model.Archivo = name_attach;

                rm = model.Save();
                if (rm.response)
                {
                    //rm.function = "DoSomething()";
                    rm.function = "LoadAttached()";
                }
            }
            else
            {
                rm.SetResponse(false, "You should attach some file");
            }


            return Json(rm);
        }

        public JsonResult DeleteAttached(int id)
        {
            var rm = new ResponseModel();
            var itemAttached = adjunto.GetOne(id);
            rm = adjunto.Delete(id);

            if (rm.response)
            {
                rm.function = "LoadAttached()";
            }

            return Json(rm);
        }
    }
}