namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Data.Entity;
    using System.Linq;

    [Table("AlumnoCurso")]
    public partial class AlumnoCurso
    {
        public int id { get; set; }

        public int Alumno_id { get; set; }

        public int Curso_id { get; set; }

        [Required(ErrorMessage = "You should type a numeric score in this field")]
        [Range(0, 20, ErrorMessage = "You should type a score between 0 and 20")]
        public decimal Nota { get; set; }

        public virtual Alumno Alumno { get; set; }

        public virtual Curso Curso { get; set; }

        public ResponseModel Save()
        {
            var rm = new ResponseModel();
            try
            {
                using (var ctx = new TestContext())
                {

                    ctx.Entry(this).State = EntityState.Added;

                    rm.SetResponse(true);
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("//********** ERROR **********//" + ex.ToString());
                throw;
            }
            return rm;
        }

        public ResponseModel Delete(int idCourse)
        {
            var rm = new ResponseModel();
            this.id = idCourse;
            try
            {
                using (var ctx = new TestContext())
                {

                    ctx.Entry(this).State = EntityState.Deleted;

                    rm.SetResponse(true);
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("//********** ERROR **********//" + ex.ToString());
                throw;
            }
            return rm;
        }

        public List<AlumnoCurso> ByStudent(int Alumno_id)
        {
            var alumnoCurso = new List<AlumnoCurso>();
            try
            {
                using (var ctx = new TestContext())
                {
                    alumnoCurso = ctx.AlumnoCurso
                        .Include("Curso")
                        .Where(x => x.Alumno_id == Alumno_id)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("************ERROR**********" + ex.ToString());
                throw;
            }
            return alumnoCurso;
        }

    }
}
