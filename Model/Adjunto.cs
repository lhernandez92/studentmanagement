namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Data.Entity;
    using System.Linq;

    [Table("Adjunto")]
    public partial class Adjunto
    {
        public int id { get; set; }

        public int Alumno_id { get; set; }

        [Required]
        [StringLength(200)]
        public string Archivo { get; set; }

        public virtual Alumno Alumno { get; set; }

        public ResponseModel Save()
        {
            var rm = new ResponseModel();
            try
            {
                using (var ctx =  new TestContext())
                {
                    ctx.Entry(this).State = EntityState.Added;
                    rm.SetResponse(true);

                    ctx.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("//********** ERROR **********//" + ex.ToString());
                throw;
            }
            return rm;
        }

        public List<Adjunto> GetAll(int idAlumno)
        {
            var adjunto = new List<Adjunto>();

            try
            {
                using (var ctx = new TestContext())
                {
                    if (idAlumno > 0)
                    {
                        adjunto = ctx.Adjunto.Where(x => x.Alumno_id == idAlumno).ToList();
                    }
                    else
                    {
                        adjunto = ctx.Adjunto.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("//********** ERROR **********//" + ex.ToString());
                throw;
            }

            return adjunto;
        }

        public ResponseModel Delete(int idAttached)
        {
            var rm = new ResponseModel();
            this.id = idAttached;
            try
            {
                using (var ctx = new TestContext())
                {

                    ctx.Entry(this).State = EntityState.Deleted;

                    rm.SetResponse(true);
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("//********** ERROR **********//" + ex.ToString());
                throw;
            }
            return rm;
        }

        public Adjunto GetOne(int idAttached)
        {
            var adjunto = new Adjunto();
            try
            {
                using (var ctx = new TestContext())
                {
                    adjunto = ctx.Adjunto.Where(x => x.id == idAttached).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("//********** ERROR **********//" + ex.ToString());
                throw;
            }

            return adjunto;
        }
    }
}
