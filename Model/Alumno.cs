namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;
    using System.Data.Entity;

    [Table("Alumno")]
    public partial class Alumno
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Alumno()
        {
            Adjunto = new HashSet<Adjunto>();
            AlumnoCurso = new HashSet<AlumnoCurso>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(100)]
        public string Apellido { get; set; }

        public int Sexo { get; set; }

        [Required]
        [StringLength(10)]
        public string FechaNacimiento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Adjunto> Adjunto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AlumnoCurso> AlumnoCurso { get; set; }

        public List<Alumno> GetList()
        {
            var alumnos = new List<Alumno>();
            try
            {
                using (var ctx = new TestContext())
                {
                    alumnos = ctx.Alumno.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("//********** ERROR **********//" + ex.ToString());
                throw;
            }

            return alumnos;
        }

        public Alumno GetOne(int idAlumno)
        {
            var alumnos = new Alumno();
            try
            {
                using (var ctx = new TestContext())
                {
                    alumnos = ctx.Alumno.Include("AlumnoCurso")
                                        .Include("AlumnoCurso.Curso")
                                        .Where(x => x.id == idAlumno)
                                        .SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("//********** ERROR **********//" + ex.ToString());
                throw;
            }

            return alumnos;
        }

        public ResponseModel Save()
        {
            var rm = new ResponseModel();
            try
            {
                using (var ctx = new TestContext())
                {
                    if (this.id > 0)
                    {
                        ctx.Entry(this).State = EntityState.Modified;
                    }
                    else
                    {
                        ctx.Entry(this).State = EntityState.Added;
                    }
                    rm.SetResponse(true);
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("//********** ERROR **********//" + ex.ToString());
                throw;
            }
            return rm;
        }

        public void Delete()
        {
            try
            {
                using (var ctx = new TestContext())
                {
                    ctx.Entry(this).State = EntityState.Deleted;
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("//********** ERROR **********//" + ex.ToString());
                throw;
            }

        }
    }
}
