namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;
    using System.Data.SqlClient;

    [Table("Curso")]
    public partial class Curso
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Curso()
        {
            AlumnoCurso = new HashSet<AlumnoCurso>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AlumnoCurso> AlumnoCurso { get; set; }

        public List<Curso> GetAll(int idAlumno = 0)
        {
            var cursos = new List<Curso>();

            try
            {
                using (var ctx = new TestContext())
                {
                    //Easy Way
                    //cursos = ctx.Database.SqlQuery<Curso>("SELECT c.* FROM Curso as c WHERE NOT IN (SELECT Curso_id FROM AlumnoCurso as ac WHERE ac.Curso_Id = c.id AND ac.Alumno_id = @Alumno_id)", new SqlParameter("Alumno_id", idAlumno)).ToList();

                    if (idAlumno > 0)
                    {
                        var courses_selected = ctx.AlumnoCurso.Where(x => x.Alumno_id == idAlumno)
                                                .Select(x => x.Curso_id)
                                                .ToList();
                        cursos = ctx.Curso.Where(x => !courses_selected.Contains(x.id)).ToList();
                    }
                    else
                    {
                        cursos = ctx.Curso.ToList();
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("************ERROR**********" + ex.ToString());
                throw;
            }

            return cursos;
        }
    }
}
